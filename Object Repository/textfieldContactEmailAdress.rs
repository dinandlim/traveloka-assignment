<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfieldContactEmailAdress</name>
   <tag></tag>
   <elementGuidId>8de5d4b9-1096-42c6-91b3-b1d30d9e8fb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Contact Details&quot;]/../div/div/div/div[2]/div[2]/div[2]/div/div/input[@aria-labelledby=&quot;emailAddress&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
