<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnRentalOfficePickupLocationFirst</name>
   <tag></tag>
   <elementGuidId>254cf225-7966-45e6-84f0-0ab39ebc15b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[text()=&quot;Pick-up Location&quot;]/../../div/following-sibling::div/div/div/div/div/div[text()=&quot;Rental Office&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
