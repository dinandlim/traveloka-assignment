<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnTapCheckReq</name>
   <tag></tag>
   <elementGuidId>d57a122f-f5aa-4927-8626-70c8ea20d5c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Rental Requirements&quot;]/../following-sibling::div/div/div/div/div[text()=&quot;Tap to check the requirements.&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
