<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfieldDriverPhoneNumber</name>
   <tag></tag>
   <elementGuidId>6033b2b9-67e3-4ca3-a855-395ab3650226</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Driver Details&quot;]/../div/div/div/div/div/div[2]/div[3]/div/input[@aria-label=&quot;Phone Number&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
