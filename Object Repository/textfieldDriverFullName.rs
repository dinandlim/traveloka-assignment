<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfieldDriverFullName</name>
   <tag></tag>
   <elementGuidId>b10bb9d0-46cf-46de-8fb6-767804a69477</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Driver Details&quot;]/../div/div/div/div/div/div[2]/div[2]/div/div/input[@aria-labelledby=&quot;name.full&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
