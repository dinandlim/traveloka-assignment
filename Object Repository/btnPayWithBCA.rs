<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnPayWithBCA</name>
   <tag></tag>
   <elementGuidId>48eede85-e29b-46d0-9a88-6bace34bf737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[text()=&quot;Pay with BCA Transfer&quot;]/following-sibling::div/div[text()=&quot;Pay with BCA Transfer&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
