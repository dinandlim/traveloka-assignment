<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnRentalOfficePickupLocationSecond</name>
   <tag></tag>
   <elementGuidId>72b9ca8b-24f0-43c7-a97b-0bcf8169fd1e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h4[text()=&quot;Pick-up Location&quot;]/../div/div/div/div/div[text()=&quot;Rental Office&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
