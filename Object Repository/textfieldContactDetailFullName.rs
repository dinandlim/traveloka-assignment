<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfieldContactDetailFullName</name>
   <tag></tag>
   <elementGuidId>78aa5d33-d273-4eb0-bdc1-745d6615b73d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Contact Details&quot;]/../div/div/div/div[2]/div/div/div/input[@aria-labelledby=&quot;name.full&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
