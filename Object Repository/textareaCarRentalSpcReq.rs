<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textareaCarRentalSpcReq</name>
   <tag></tag>
   <elementGuidId>92dc2023-7eff-4c50-93a2-59076e9af764</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Car Rental Special Request (optional)&quot;]/../following-sibling::div/div/div/div[3]/div/textarea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
