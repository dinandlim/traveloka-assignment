<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtDropOffOptionalNotes</name>
   <tag></tag>
   <elementGuidId>3c723a9e-cb9e-4e40-9c1f-5d7e21bdde24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/textarea[@placeholder=&quot;Additional notes (optional)&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
