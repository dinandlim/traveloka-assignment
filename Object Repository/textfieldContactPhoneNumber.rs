<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfieldContactPhoneNumber</name>
   <tag></tag>
   <elementGuidId>fc9599d2-4ef1-4ef2-aba5-5f85e5d7e5da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h2[text()=&quot;Contact Details&quot;]/../div/div/div/div[2]/div[2]/div/div/input[@aria-label=&quot;Phone Number&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
