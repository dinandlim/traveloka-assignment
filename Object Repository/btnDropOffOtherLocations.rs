<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnDropOffOtherLocations</name>
   <tag></tag>
   <elementGuidId>975ae7b6-d69a-49a5-9f7e-fe7d83b1bf92</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[text()=&quot;Drop-off Location&quot;]/../../div/following-sibling::div/div/div/div/div/div[text()=&quot;Other Locations&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
