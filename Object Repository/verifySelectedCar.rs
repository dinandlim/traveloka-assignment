<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verifySelectedCar</name>
   <tag></tag>
   <elementGuidId>836ac780-748e-4b7c-90e6-3bd5436e40ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[text()=&quot;Car Type&quot;]/following-sibling::h4[text()=&quot;Daihatsu Ayla&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
