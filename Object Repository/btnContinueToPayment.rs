<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnContinueToPayment</name>
   <tag></tag>
   <elementGuidId>1cd7f1fa-bf2c-40f3-970c-7e6ab6c0a8f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[text()=&quot;Continue to Payment&quot;]/following-sibling::div/div[text()=&quot;Continue to Payment&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
