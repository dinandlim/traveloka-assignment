import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import java.net.MalformedURLException as MalformedURLException
import java.text.Format as Format
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date


//Select Cars Product
WebUI.waitForElementClickable(findTestObject('btnCarRental'), 0)

WebUI.click(findTestObject('btnCarRental'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('btnWithoutDriver'), 0)

//Select tab Without Driver
WebUI.click(findTestObject('btnWithoutDriver'), FailureHandling.STOP_ON_FAILURE)

//Select Pick-up Location (e.g.: Jakarta)
WebUI.sendKeys(findTestObject('txtfieldRentalLocation'), 'Jakarta')

Thread.sleep(2000)

WebUI.waitForElementClickable(findTestObject('btnRentalLocation'), 0)

WebUI.click(findTestObject('btnRentalLocation'), FailureHandling.STOP_ON_FAILURE)

//Select Pick-up Date & Time (e.g.: today+2d 09:00)
Format formatter = new SimpleDateFormat('dd MMMM')

String TodayDate = formatter.format(new Date())

print(TodayDate)

String string = TodayDate

String[] parts = string.split(' ')

String txtDay = parts[0]

String txtMonth = parts[1]

int intDayStart = Integer.parseInt(txtDay) + 2

int intDayEnd = Integer.parseInt(txtDay) + 5

print(intDayEnd)

WebUI.click(findTestObject('btnRentalStartDate'), FailureHandling.STOP_ON_FAILURE)

btnChooseStartDay = new TestObject('')

btnChooseStartDay.addProperty('xpath', ConditionType.EQUALS, ((('//h3[contains(.,"' + txtMonth) + '")]/../../../div[3]/div/div/div/div/div/div[text()="') + 
    intDayStart) + '"]')

WebUI.click(btnChooseStartDay)

WebUI.click(findTestObject('btnRentalEndDate'), FailureHandling.STOP_ON_FAILURE)

//Select Drop-off Date & Time (e.g.: today+5d 11:00)
btnChooseEndDay = new TestObject('')

btnChooseEndDay.addProperty('xpath', ConditionType.EQUALS, ('(//h3[contains(.,"February")]/../../../div[3]/div/div/div/div/div/div[text()="' + 
    intDayEnd) + '"])[last()]')

WebUI.click(btnChooseEndDay)

WebUI.click(findTestObject('btnEndTime'))

btnEndTime = new TestObject('')

btnEndTime.addProperty('xpath', ConditionType.EQUALS, '//div[text()="Hour"]/../div/div/div/div/div[text()="11"]')

WebUI.scrollToElement(btnEndTime, 0)

WebUI.scrollToPosition(0, 0)

WebUI.click(btnEndTime)

WebUI.click(findTestObject('btnDoneEndTime'))

//Click button Search Car
WebUI.click(findTestObject('btnSearchButton'))

WebUI.waitForPageLoad(0, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Car Rental Without Driver', false)

//Select Car
selectedCar = 'Daihatsu Ayla'

btnSelectedCar = new TestObject('')

btnSelectedCar.addProperty('xpath', ConditionType.EQUALS, ('//h3[text()="' + selectedCar) + '"]/../../following-sibling::div/div[3]/div/div/div[text()="Continue"]')

WebUI.click(btnSelectedCar)

verifySelectedCar = new TestObject('')

verifySelectedCar.addProperty('xpath', ConditionType.EQUALS, ('//div[text()="Car Type"]/following-sibling::h4[text()="' + 
    selectedCar) + '"]')

WebUI.waitForElementVisible(verifySelectedCar, 0)

WebUI.verifyElementPresent(verifySelectedCar, 0)

//Select Car Provider
selectedCarProvider = 'Jayamahe Easy Ride Jakarta'

btnSelectedCarProvider = new TestObject('')

btnSelectedCarProvider.addProperty('xpath', ConditionType.EQUALS, '//h3[text()="Jayamahe Easy Ride Jakarta"]/../../following-sibling::div/div[2]/div/div[text()="Continue"]/../div/div')

WebUI.click(btnSelectedCarProvider)

verifySelectedCarProductDetailPage = new TestObject('')

verifySelectedCarProductDetailPage.addProperty('xpath', ConditionType.EQUALS, ('//h2[text()="' + selectedCar) + '"]')

WebUI.verifyElementPresent(verifySelectedCarProductDetailPage, 0)

verifySelectedCarProviderProductDetailPage = new TestObject('')

verifySelectedCarProviderProductDetailPage.addProperty('xpath', ConditionType.EQUALS, ('//h3[text()="' + selectedCarProvider) + 
    '"]')

WebUI.verifyElementPresent(verifySelectedCarProviderProductDetailPage, 0)

//Click button Continue in Product Detail
WebUI.click(findTestObject('btnContinue'))

WebUI.verifyTextPresent('Please check your booking', false)

// Select Pick-up Location in “Rental Office”
WebUI.scrollToElement(findTestObject('txtCarProvider'), 0)

WebUI.click(findTestObject('btnRentalOfficePickupLocationFirst'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('btnRentalOfficePickupLocationSecond'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('btnCarProviderPool'), FailureHandling.STOP_ON_FAILURE)

//Select Drop-off Location in “Other Location”
WebUI.click(findTestObject('btnDropOffOtherLocations'))

WebUI.sendKeys(findTestObject('txtfieldLocationAndress'), 'Soekarno')

WebUI.click(findTestObject('btnTextChoosenDropOffLocation'))

//Input Pick-up/Drop-off notes is optional
WebUI.sendKeys(findTestObject('txtDropOffOptionalNotes'), 'Testing Optional Notes')

//Click button Book Now
WebUI.scrollToElement(findTestObject('btnContinue'), 0)

WebUI.click(findTestObject('btnContinue'))

//Fill Contact Details
WebUI.waitForElementPresent(findTestObject('textfieldContactDetailFullName'), 0)

WebUI.sendKeys(findTestObject('textfieldContactDetailFullName'), 'Ferdinand')

WebUI.sendKeys(findTestObject('textfieldContactPhoneNumber'), '812345678')

WebUI.sendKeys(findTestObject('textfieldContactEmailAdress'), 'testing@gmail.com')

//Fill Driver Details
WebUI.selectOptionByValue(findTestObject('dropdownDriverTitle'), 'MR', false)

WebUI.sendKeys(findTestObject('textfieldDriverFullName'), 'DriverTest')

WebUI.sendKeys(findTestObject('textfieldDriverPhoneNumber'), '81212121212')

//Click Continue
WebUI.scrollToElement(findTestObject('btnContinue'), 0)

WebUI.click(findTestObject('btnContinue'))

//Add a special request is optional
WebUI.scrollToElement(findTestObject('textareaCarRentalSpcReq'), 0)

WebUI.sendKeys(findTestObject('textareaCarRentalSpcReq'), 'Test Special Request')

//Check all rental requirements
WebUI.click(findTestObject('btnTapCheckReq'))

WebUI.waitForElementVisible(findTestObject('btnCheckAllRentalReq'), 0)

WebUI.enhancedClick(findTestObject('btnCheckAllRentalReq'))

WebUI.scrollToElement(findTestObject('btnSaveRentalReq'), 0)

WebUI.click(findTestObject('btnSaveRentalReq'))

//Click Continue
WebUI.click(findTestObject('btnContinueToPayment'))

WebUI.waitForElementVisible(findTestObject('btnContinue'), 0)

WebUI.click(findTestObject('btnContinue'))

//Select payment method and proceed payment
WebUI.waitForElementVisible(findTestObject('btnBankTransfer'), 0)

WebUI.click(findTestObject('btnBankTransfer'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('btnBCATransfer'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('btnPayWithBCA'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent('Completed your payment?', false)

